﻿using System.IO;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using RecruiterProfileScraper.ScraperMethods;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Resources;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services.Discovery;
using HtmlAgilityPack;
using RecruiterProfileScraper.Models;

namespace RecruiterProfileScraper.Controllers
{
    public class HomeController : Controller
    {
        private ProfileScraper profileScraper;
        private SearchProfileContent searchProfileContent;

        public HomeController()
        {
            profileScraper = new ProfileScraper();
            searchProfileContent = new SearchProfileContent();
        }

        


        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ProfileScraper()
        {
            // Clear all tables before starting the scan
            profileScraper.ClearAllTables();

            // Get number of pages from directory
            var numberOfDirectsPages = profileScraper.GetPageNumbersDirects();
            int numberOfDirectsPagesInt = int.Parse(numberOfDirectsPages);

            // create urls and save them to a text file
            profileScraper.CreateRecruiterDirectoryUrlsDirects(numberOfDirectsPagesInt);



            // Get number of pages from directory
            var numberOfAgenciesPages = profileScraper.GetPageNumbersAgencies();
            int numberOfAgenciesPagesInt = int.Parse(numberOfAgenciesPages);

            // create urls and save them to a text file
            profileScraper.CreateRecruiterDirectoryUrlsAgencies(numberOfAgenciesPagesInt);

            

            // Get all of the links to recruiters jobs pages
            profileScraper.GetSilverProfileLinks();

            // Get the urls to the recruiter profiles from the "Find out more" links on the recruiters jobs pages
            profileScraper.GetFindOutMoreLinks();

            var listOfProfileLinks = profileScraper.GetListOfProfileLinks();

            // scan all of the silver profiles for web and email addresses and telephone numbers
            var listOfResults = searchProfileContent.ScanProfileForLinks(listOfProfileLinks);

            return View(listOfResults);
        }
    }
}