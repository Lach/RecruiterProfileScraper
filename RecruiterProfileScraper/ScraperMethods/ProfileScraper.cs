﻿using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Net;
using System.Web.WebPages;
using Dapper;
using HtmlAgilityPack;
using System;
using System.IO;
using System.Linq;
using System.Web;
using Microsoft.Ajax.Utilities;
using RecruiterProfileScraper.Models;
using System.Text.RegularExpressions;
using System.Text;

namespace RecruiterProfileScraper.ScraperMethods
{
    public class ProfileScraper
    {
        // Clear all links from last scan
        public void ClearAllTables()
        {
            const string deleteRecruiterDirectoryTable = @"DELETE FROM dbo.RecruiterDirectory";
            const string deleteRecruiterProfileDirectoryTable = @"DELETE FROM dbo.RecruiterProfileDirectory";
            const string deleteSilverProfileDirectoryTable = @"DELETE FROM dbo.SilverProfileDirectory";
            const string deleteInvalidItemsTable = @"DELETE FROM dbo.InvalidContent";

            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["RecruiterProfileDirectory"].ConnectionString)
                )
            {
                conn.ExecuteScalar<int>(deleteRecruiterDirectoryTable);
                conn.ExecuteScalar<int>(deleteRecruiterProfileDirectoryTable);
                conn.ExecuteScalar<int>(deleteSilverProfileDirectoryTable);
                conn.ExecuteScalar<int>(deleteInvalidItemsTable);
            }
        }

        public string GetPageNumbersDirects()
        {
            const string directoryDirects = "http://www.reed.co.uk/recruiterdirectory";
            const string directoryAgencies = "http://www.reed.co.uk/recruiterdirectory/agencies";

            List<string> paginationList = new List<string>();

            var openRecruiterDirectoryDirects = new HtmlDocument();
            openRecruiterDirectoryDirects.LoadHtml(new WebClient().DownloadString(directoryDirects));

            HtmlNode paginationContainerDiv = openRecruiterDirectoryDirects.DocumentNode.SelectSingleNode("//div[@class='pages']");

            if (paginationContainerDiv != null)
            {
                var links = paginationContainerDiv.Descendants("a").Select(a => a.InnerText);

                foreach (var result in links)
                {
                    paginationList.Add(result);
                }
            }
            else
            {
                return null;
            }

            var numberOfPages = paginationList[4];

            return numberOfPages;
        }

        public string GetPageNumbersAgencies()
        {
            const string directoryAgencies = "http://www.reed.co.uk/recruiterdirectory/agencies";

            List<string> paginationList = new List<string>();

            var openRecruiterDirectoryDirects = new HtmlDocument();
            openRecruiterDirectoryDirects.LoadHtml(new WebClient().DownloadString(directoryAgencies));

            HtmlNode paginationContainerDiv = openRecruiterDirectoryDirects.DocumentNode.SelectSingleNode("//div[@class='pages']");

            if (paginationContainerDiv != null)
            {
                var links = paginationContainerDiv.Descendants("a").Select(a => a.InnerText);

                foreach (var result in links)
                {
                    paginationList.Add(result);
                }
            }
            else
            {
                return null;
            }

            var numberOfPages = paginationList[4];

            return numberOfPages;
        }

        public void CreateRecruiterDirectoryUrlsDirects(int numberOfPages)
        {
            var recruiterDirectoryHomeUrl = "http://www.reed.co.uk/recruiterdirectory";

            List<string> listOfDirectoryUrls = new List<string>();

            for (int i = 1; i <= numberOfPages; i++)
            {
                var url = recruiterDirectoryHomeUrl + "?pageno=" + i;
                listOfDirectoryUrls.Add(url);
                //RecruiterSilverProfileUrls(url);
            }
            SaveRecruiterDirectoryUrls(listOfDirectoryUrls);
        }

        public void CreateRecruiterDirectoryUrlsAgencies(int numberOfPages)
        {
            var recruiterDirectoryHomeUrl = "http://www.reed.co.uk/recruiterdirectory/agencies";

            List<string> listOfDirectoryUrls = new List<string>();

            for (int i = 1; i <= numberOfPages; i++)
            {
                var url = recruiterDirectoryHomeUrl + "?pageno=" + i;
                listOfDirectoryUrls.Add(url);
                //RecruiterSilverProfileUrls(url);
            }
            SaveRecruiterDirectoryUrls(listOfDirectoryUrls);
        }

        public void GetSilverProfileLinks()
        {
            List<string> recruiterDirectoryLinks = GetRecruiterDirectoryUrls();

            // Loop through each directory page and get all of the profile links

            List<string> silverProfileLinks = new List<string>();

            foreach (var link in recruiterDirectoryLinks)
            {
                try
                {
                    var openRecruiterDirectory = new HtmlDocument();
                    openRecruiterDirectory.LoadHtml(new WebClient().DownloadString(link));

                    var profileThumbAnchors =
                        openRecruiterDirectory.DocumentNode.SelectNodes("//div[@class='profileThumb']/a");

                    if (profileThumbAnchors != null)
                    {
                        foreach (var anchor in profileThumbAnchors)
                        {
                            var recruiterSilverProfileUrl = "http://www.reed.co.uk" + anchor.GetAttributeValue("href", "");
                            silverProfileLinks.Add(recruiterSilverProfileUrl);
                        }
                    }
                }
                catch
                {
                }
            }
         SaveRecruiterSilverProfileLinks(silverProfileLinks);
        }

        public void GetFindOutMoreLinks()
        {
            // get silver profile links from db
            List<string> listOfSilverProfileLinks = GetRecruiterSilverProfileLinks();
            
            List<string> listOfRecruiterProfiles = new List<string>();

            foreach (var link in listOfSilverProfileLinks)
            {
                try
                {
                    var openRecruiterDirectory = new HtmlDocument();
                    openRecruiterDirectory.LoadHtml(new WebClient().DownloadString(link));

                    var linkToRecruiterProfile =
                        openRecruiterDirectory.DocumentNode.SelectNodes("//div[@class='findOutMore']/a");

                    if (linkToRecruiterProfile != null)
                    {
                        foreach (var url in linkToRecruiterProfile)
                        {
                            var recruiterProfileUrl = "http://www.reed.co.uk" + url.GetAttributeValue("href", "");
                            listOfRecruiterProfiles.Add(recruiterProfileUrl);
                        }
                    }
                }
                catch
                {
                }

            }
            SaveRecruiterProfileLinks(listOfRecruiterProfiles);
        }

        

        private void SaveRecruiterDirectoryUrls(List<string> listOfDirectoryUrls)
        {
            foreach (var link in listOfDirectoryUrls)
            {
                const string sql = @"INSERT INTO RecruiterDirectory(Links)" + "VALUES(@Links)";
                var param = new {Links = link};

                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["RecruiterProfileDirectory"].ConnectionString))
                {
                    conn.ExecuteScalar<int>(sql, param);
                }
            }
        }

        private List<string> GetRecruiterDirectoryUrls()
        {
            const string sql = "SELECT Links FROM RecruiterDirectory";

            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["RecruiterProfileDirectory"].ConnectionString))
            {
                return conn.Query<string>(sql).ToList();
            }
        }

        private void SaveRecruiterSilverProfileLinks(List<string> silverProfileLinks)
        {
            foreach (var link in silverProfileLinks)
            {
                const string sql = @"INSERT INTO SilverProfileDirectory(Links)" + "VALUES(@Links)";
                var param = new { Links = link };

                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["RecruiterProfileDirectory"].ConnectionString))
                {
                    conn.ExecuteScalar<int>(sql, param);
                }
            }
        }

        private List<string> GetRecruiterSilverProfileLinks()
        {
            const string sql = "SELECT Links FROM SilverProfileDirectory";

            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["RecruiterProfileDirectory"].ConnectionString))
            {
                return conn.Query<string>(sql).ToList();
            }
        }

        private void SaveRecruiterProfileLinks(List<string> silverProfileLinks)
        {
            foreach (var link in silverProfileLinks)
            {
                const string sql = @"INSERT INTO RecruiterProfileDirectory(Links)" + "VALUES(@Links)";
                var param = new { Links = link };

                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["RecruiterProfileDirectory"].ConnectionString))
                {
                    conn.ExecuteScalar<int>(sql, param);
                }
            }
        }

        public List<string> GetListOfProfileLinks()
        {
            const string sql = "SELECT Links FROM RecruiterProfileDirectory";

            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["RecruiterProfileDirectory"].ConnectionString))
            {
                return conn.Query<string>(sql).ToList();
            }
        } 
        
    }
}