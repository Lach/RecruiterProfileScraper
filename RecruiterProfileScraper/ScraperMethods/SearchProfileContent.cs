﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using Dapper;
using HtmlAgilityPack;
using RecruiterProfileScraper.Models;

namespace RecruiterProfileScraper.ScraperMethods
{
    public class SearchProfileContent
    {

        public List<ErrorsModel> ScanProfileForLinks(List<string> listOfProfileLinks)
        {
            List<string> profileTabLinks = new List<string>();

            foreach (var link in listOfProfileLinks)
            {
                //const string profileLink = "http://www.reed.co.uk/profile/wesleyan-assurance-society/about-wesleyan";
                //const string profileLink = "http://test.reedlabs.co.uk/profile/tesco-7156?view=draft";
                //const string profileLink = "http://test.reedlabs.co.uk/profile/thorpe-park-6066?view=draft";
                //const string profileLink = "http://test.reedlabs.co.uk/profile/thames-valley-police/about-us?view=draft";

                var openRecruiterDirectoryDirects = new HtmlDocument();
                openRecruiterDirectoryDirects.LoadHtml(new WebClient().DownloadString(link));

                profileTabLinks.Add(link);

                try
                {
                    var profileTabs = openRecruiterDirectoryDirects.DocumentNode.SelectNodes("//ul[@class='recruiterMenu']/li/a");

                    foreach (var li in profileTabs)
                    {
                        var links = "http://www.reed.co.uk" + li.GetAttributeValue("href", "");
                        profileTabLinks.Add(links);
                    }
                }
                catch
                {
                }
            }
            return SelectContentFromPages(profileTabLinks);
        }

        public List<ErrorsModel> SelectContentFromPages(List<string> profileTabLinks)
        {
            List<ErrorsModel> invalidItems = new List<ErrorsModel>();

            foreach (var link in profileTabLinks)
            {
                try
                {
                    var openRecruiterProfile = new HtmlDocument();
                    openRecruiterProfile.LoadHtml(new WebClient().DownloadString(link));

                    var mainCol = openRecruiterProfile.DocumentNode.SelectNodes("//div[@class='recruiter_resultsCol']");

                    if (mainCol != null)
                    {
                        List<string> profileContent = new List<string>();

                        var profileTabs = openRecruiterProfile.DocumentNode.SelectNodes("//div[@class='recruiter_resultsCol']//*");

                        // Save each node to list profileContent
                        foreach (var node in profileTabs)
                        {
                            var profileText = node.InnerText;
                            if (profileText != null)
                            {
                                profileContent.Add(profileText);
                            }
                        }

                        // Loop through each node in profileContent list and check for invalid content (Email addresses, web addresses and telephone numbers)
                        // Save to invalidItems list
                        foreach (var blockOfText in profileContent)
                        {
                            // Email addresses
                            var resultEmailAddresses = searchForEmailAddresses(blockOfText);
                            foreach (var email in resultEmailAddresses)
                            {
                                var emailAddresses = new ErrorsModel(email, link, "EmailAddress");
                                invalidItems.Add(emailAddresses);
                            }
                            // Telephone numbers
                            var resultTelephoneNumbers = searchForTelephoneNumbers(blockOfText);
                            foreach (var phoneNumber in resultTelephoneNumbers)
                            {
                                var telephoneNumbers = new ErrorsModel(phoneNumber, link, "PhoneNumber");
                                invalidItems.Add(telephoneNumbers);
                            }
                            // External Links
                            var webAddresses = searchForWebAddresses(blockOfText);
                            foreach (var webAddress in webAddresses) 
                            {
                                var webAddressesFound = new ErrorsModel(webAddress, link, "WebAddress");
                                invalidItems.Add(webAddressesFound);
                            }
                        }
                    }
                    SaveInvalidItems(invalidItems);
                }
                catch
                {
                }
            }
            return invalidItems;
        }
        
        private List<string> searchForEmailAddresses(string blockOfText)
        {
            List<string> listOfEmailAdresses = new List<string>();

            const string matchEmailPattern = @"(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
          + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
            + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
          + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})";

            Regex rxEmailAddress = new Regex(matchEmailPattern, RegexOptions.Compiled | RegexOptions.IgnoreCase);

            MatchCollection emailAddressMatches = rxEmailAddress.Matches(blockOfText);

            // number of matches found.
            int noOfEmailAddressMatches = emailAddressMatches.Count;

            foreach (Match emailAddress in emailAddressMatches)
            {
                string result = emailAddress.Value;
                listOfEmailAdresses.Add(result);
            }
            return listOfEmailAdresses;
        }

        private List<string> searchForTelephoneNumbers(string blockOfText)
        {
            List<string> listOfTelephoneNumbers = new List<string>();

            const string matchTelephoneNumbers = @"((\d{3,5})\s(\d{3,5})\s(\d{3,5})|(\d{3,5})-(\d{3,5})-(\d{3,5})|(\d{3,5}).(\d{3,5}).(\d{3,5})|(\d{11}))|(\d{4,6}\s\d{3,6})";

            Regex rxTelephoneNumber = new Regex(matchTelephoneNumbers, RegexOptions.Compiled | RegexOptions.IgnoreCase);

            MatchCollection telephoneNumberMatches = rxTelephoneNumber.Matches(blockOfText);

            // number of matches found.
            int noOfEmailAddressMatches = telephoneNumberMatches.Count;

            foreach (Match emailAddress in telephoneNumberMatches)
            {
                string result = emailAddress.Value;
                listOfTelephoneNumbers.Add(result);
            }
            return listOfTelephoneNumbers;
        }

        private List<string> searchForWebAddresses(string blockOfText)
        {
            List<string> listOfWebAddresses = new List<string>();

            const string matchTelephoneNumbers = @"(https|http|www)[!'#$%&'()*+,./:;<=>?@\^_`{|}~-]+[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?";

            Regex rxWebAddresses = new Regex(matchTelephoneNumbers, RegexOptions.Compiled | RegexOptions.IgnoreCase);

            MatchCollection webAddressMatches = rxWebAddresses.Matches(blockOfText);

            // number of matches found.
            int noOfEmailAddressMatches = webAddressMatches.Count;

            foreach (Match emailAddress in webAddressMatches)
            {
                string result = emailAddress.Value;
                listOfWebAddresses.Add(result);
            }
            return listOfWebAddresses;
        } 
        
        private void SaveInvalidItems(List<ErrorsModel> invalidItems)
        {
            foreach (var item in invalidItems)
            {
                const string sql = "INSERT INTO dbo.InvalidContent(Id, InvalidItem, PageLink, ContentType) " + "VALUES (@Id, @InvalidItem, @PageLink, @ContentType)";
                var param =
                    new
                    {
                        item.Id,
                        item.InvalidItem,
                        item.PageLink,
                        item.ContentType
                    };

                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["RecruiterProfileDirectory"].ConnectionString))
                {
                    conn.ExecuteScalar<int>(sql, param);
                }
            }
        }
    }
}