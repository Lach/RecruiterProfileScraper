﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RecruiterProfileScraper.Models
{
    public class RecruiterDirectory
    {
        public int Id { get; set; }
        public string Links { get; set; }

        public RecruiterDirectory(int id, string links)
        {
            Id = id;
            Links = links;
        }
    }
}