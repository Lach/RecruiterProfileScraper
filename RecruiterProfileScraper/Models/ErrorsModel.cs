﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RecruiterProfileScraper.Models
{
    public class ErrorsModel
    {
        public int Id { get; set; }
        public string InvalidItem { get; set; }
        public string PageLink { get; set; }
        public string ContentType { get; set; }

        public ErrorsModel(string invalidItem, string pageLink, string contentType)
        {
            InvalidItem = invalidItem;
            PageLink = pageLink;
            ContentType = contentType;
        }

        public ErrorsModel()
        {
            
        }
    }
}