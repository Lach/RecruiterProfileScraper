﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RecruiterProfileScraper.Models
{
    public class RecruiterProfileModel
    {
        public int Id { get; set; }
        public string ProfileLinks { get; set; }
        public string RecruiterAddress { get; set; }
        public string RecruiterTelephone { get; set; }
        public string RecruiterEmailAddress { get; set; }
        public string RecruiterWebSiteLinks { get; set; }

        public RecruiterProfileModel(int id, string profileLinks, string recruiterAddress, string recruiterTelephone, string recruiterEmailAddress, string recruiterWebSiteLinks)
        {
            Id = id;
            ProfileLinks = profileLinks;
            RecruiterAddress = recruiterAddress;
            RecruiterTelephone = recruiterTelephone;
            RecruiterEmailAddress = recruiterEmailAddress;
            RecruiterWebSiteLinks = recruiterWebSiteLinks;
        }

    }
}