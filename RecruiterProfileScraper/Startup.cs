﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RecruiterProfileScraper.Startup))]
namespace RecruiterProfileScraper
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
